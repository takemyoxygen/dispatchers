using System;
using System.Threading;
using System.Threading.Tasks;

namespace Dispatchers
{
    public class TaskContinuationDispatcher : IDispatcher
    {
        private Task root;

        private readonly object gate = new object();
        
        public TaskContinuationDispatcher()
        {
            this.root = Task.FromResult(42);
        }

        public void BeginInvoke(Action action)
        {
            lock (this.gate)
            {
                this.root = this.root.ContinueWith(_ => action());
            }
        }

        public void Dispose()
        {
            this.root.Dispose();
        }
    }
}