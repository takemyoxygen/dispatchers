using System;
using System.Collections.Concurrent;
using System.Threading;

namespace Dispatchers
{
    public class DedicatedThreadDispatcher : IDispatcher
    {
        private readonly ConcurrentQueue<Action> actions = new ConcurrentQueue<Action>();

        private volatile bool disposed;

        private readonly Thread worker;

        public DedicatedThreadDispatcher()
        {
            this.worker = new Thread(this.Loop) { IsBackground = true };
            this.worker.Start();
        }

        private void Loop()
        {
            while (!this.disposed)
            {
                Action next;
                if (this.actions.TryDequeue(out next))
                {
                    next();
                }
            }
        }

        public void BeginInvoke(Action action)
        {
            if (this.disposed)
            {
                throw new ObjectDisposedException(this.GetType().Name);
            }

            this.actions.Enqueue(action);
        }

        public void Dispose()
        {
            this.disposed = true;
            this.worker.Join();
        }
    }
}