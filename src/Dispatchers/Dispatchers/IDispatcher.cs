using System;

namespace Dispatchers
{
    public interface IDispatcher : IDisposable
    {
        void BeginInvoke(Action action);
    }
}