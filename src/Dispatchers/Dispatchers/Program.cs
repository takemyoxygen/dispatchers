﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;

namespace Dispatchers
{
    class Program
    {
        static void Main(string[] args)
        {
            const int operations = 1000000;
            const int dispatchers = 40;

            Console.WriteLine("Creating Stopwatches...");

            var watches = Enumerable
                .Range(1, operations)
                .Select(x => new Stopwatch())
                .ToArray();

            Test(() => new DedicatedThreadDispatcher(), watches, dispatchers);
            GC.Collect(GC.MaxGeneration);

            Test(() => new TaskContinuationDispatcher(), watches, dispatchers);
            GC.Collect(GC.MaxGeneration);

            Test(() => new ThreadPoolDispatcher(), watches, dispatchers);
            GC.Collect(GC.MaxGeneration);
        }

        static void Test(Func<IDispatcher> dispatcher, Stopwatch[] timers, int dispatchersCount)
        {
            Console.WriteLine("Creating dispatchers...");

            var dispatchers = Enumerable
                .Range(1, dispatchersCount)
                .Select(_ => dispatcher())
                .ToArray();

            foreach (var timer in timers)
            {
                timer.Reset();
            }

            Console.WriteLine("Dispatcher: " + dispatchers[0].GetType().Name);

            TimeSpan total;
            var delays = GetDelays(dispatchers, timers, out total);

            var max = delays.Max();
            var avg = TimeSpan.FromTicks((long)delays.Average(t => t.Ticks));

            Console.WriteLine("Delays: max: {0}, avg: {1}", max, avg);
            Console.WriteLine("Total threads: " + Process.GetCurrentProcess().Threads.Count);
            Console.WriteLine("Total time: " + total);

            Console.WriteLine();
                
            foreach (var d in dispatchers) d.Dispose();
        }

        static ICollection<TimeSpan> GetDelays(IDispatcher[] dispatchers, Stopwatch[] timers, out TimeSpan totalTime)
        {
            var total = new Stopwatch();
            total.Start();

            Parallel.ForEach(
                timers,
                (sw, state, idx) =>
                {
                    sw.Start();
                    dispatchers[idx % dispatchers.Length].BeginInvoke(sw.Stop);
                });

            Observable
                .Interval(TimeSpan.FromSeconds(0.1))
                .Select(_ => timers.All(t => !t.IsRunning))
                .FirstOrDefaultAsync(done => done)
                .Wait();

            total.Stop();

            totalTime = total.Elapsed;

            return timers.Select(sw => sw.Elapsed).ToArray();
        }
    }
}
