using System;
using System.Collections.Generic;
using System.Threading;

namespace Dispatchers
{
    public class ThreadPoolDispatcher : IDispatcher
    {
        private readonly Queue<Action> queue = new Queue<Action>();

        private bool running = false;

        private readonly object gate = new object();

        public void Dispose()
        {
        }

        public void BeginInvoke(Action action)
        {
            Action next = null;

            lock (this.gate)
            {
                if (this.running)
                {
                    this.queue.Enqueue(action);
                }
                else
                {
                    this.running = true;
                    next = action;
                }
            }

            if (next != null)
            {
                this.Schedule(next);
            }
        }

        private void Schedule(Action a)
        {
            ThreadPool.UnsafeQueueUserWorkItem(
                _ =>
                {
                    a();
                    Action next = null;
                    lock (this.gate)
                    {
                        if (this.queue.Count > 0)
                        {
                            next = this.queue.Dequeue();
                        }
                        else
                        {
                            this.running = false;
                        }
                        
                    }
                    if (next != null)
                    {
                        this.Schedule(next);
                    }
                },
                null);
        }
    }
}