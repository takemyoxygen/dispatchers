## Performance comparison of different ways to invoke an action on another thread.

Dispatched operations: 1 000 000

Dispatchers: 40

```
Dispatcher: DedicatedThreadDispatcher
Delays: max: 00:00:04.0951923, avg: 00:00:00.2917358
Total threads: 52
Total time: 00:00:14.1444759

Creating dispatchers...
Dispatcher: TaskContinuationDispatcher
Delays: max: 00:00:01.9789782, avg: 00:00:00.9933019
Total threads: 28
Total time: 00:00:02.1026347

Creating dispatchers...
Dispatcher: ThreadPoolDispatcher
Delays: max: 00:00:00.4948185, avg: 00:00:00.3382885
Total threads: 28
Total time: 00:00:00.6339048
```